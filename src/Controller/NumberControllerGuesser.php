<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class NumberControllerGuesser extends AbstractController
{
    /**
     * @Route("/ghicireNumar", name="ghicireNumbar")
     */
    public function number()
    {
        session_start();
        $numarul = rand(0,100);

        $_SESSION['numar'] = $numarul;

        return $this->render('index.html.twig', [
            'text' => "numarul trimis este in sesiune",
            'numaruDinSesiune' => $_SESSION['numar'],
        ]);
    }
}
